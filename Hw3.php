<!DOCTYPE html>  
<html>   
<head>   
<title>create table Hm3</title>   
<meta charset="utf-8">  
</head>   
<body>   
<h1>create table Hm3</h1>   

<?php   

include 'dbconnect.php';  //connect to DB  
//Droping The tables  
//Please write them in the right order not to have foreign key valuation.  
$dropping = @mysql_query('DROP TABLE IF EXISTS orderDetails, orders, customers ;');  
if (!$dropping) {   
  exit('<p>Error dropping the tables<br />'.   
      'Error: ' . mysql_error() . '</p>');   
}   

if ($dropping) {   
  echo 'everything went just fine dropping the tables<br>';   
}   


//CONSTRAINT FOREIGN KEY (customerId) REFERENCES customers(customerId) ON DELETE CASCADE ON UPDATE CASCADE 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
//create the customers table  
$customers = @mysql_query('CREATE TABLE IF NOT EXISTS customers(customerId    CHAR(9),  
                                                                name    CHAR(20),  
                                                                adress CHAR(50),  
                                                            PRIMARY KEY (customerId))  
                        ENGINE innoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;');   
if (!$customers) {   
  exit('<p>Error creating the customers table<br />'.   
      'Error: ' . mysql_error() . '</p>');   
}  
if ($customers) {   
  echo 'everything went just fine with customers table <br>';   
}   

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
//create the orders table  
$orders = @mysql_query('CREATE TABLE IF NOT EXISTS orders (orderId    CHAR(9),  
                                                                date    Date,  
                                                                customerId    CHAR(9) NOT NULL, 
                                                                PRIMARY KEY (orderId), 
                                                                CONSTRAINT FOREIGN KEY (customerId) REFERENCES customers(customerId) ON DELETE CASCADE ON UPDATE CASCADE) 
                                    ENGINE innoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;');  
                                     
if (!$orders) {   
  exit('<p>Error creating the orders table<br />'.   
      'Error: ' . mysql_error() . '</p>');   
}  
if ($orders) {   
  echo 'everything went just fine with orders table <br>';   
}   



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
//create the orderDetails table  
$orderDetails = @mysql_query('CREATE TABLE IF NOT EXISTS orderDetails (productId   CHAR(9),  
                                                            orderId   CHAR(9),     
                                                            quantity INT, 
                                                            PRIMARY KEY (orderId,productId),  
                                                            CONSTRAINT FOREIGN KEY (orderId) REFERENCES orders(orderId) ON DELETE CASCADE ON UPDATE CASCADE,  
                                                            CONSTRAINT FOREIGN KEY (productId) REFERENCES products(productId) ON DELETE CASCADE ON UPDATE CASCADE)  
                            ENGINE innoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;');   
if (!$orderDetails) {   
  exit('<p>Error creating the orderDetails table<br />'.   
      'Error: ' . mysql_error() . '</p>');   
}  
if ($orderDetails) {   
  echo 'everything went just fine with orderDetails table <br>';   
}   
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  

?>   
</body>   
</html> 